import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  // valeur par defaut de enonce = TATA si on met input, input va aussi autoriser a passer une variable dans balise question
  @Input() enonce = 'question par defaut';
  @Input() reponse1: string ;
  @Input() reponse2: string ;
  @Input() reponseUser: string;

jaiClick(reponse: string) {
 this.reponseUser = reponse;
}

  constructor() { }

ngOnInit() {
  }

}
