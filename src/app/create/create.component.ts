import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Question} from '../model/question';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  // output permet de l'exterioriser va créer un event qui va émettre
  @Output()  questionCreated = new EventEmitter();
  inputValue = '';
  inputValueChoice1 = '' ;
  inputValueChoice2 = '';
  alert: string;
  finalValue: string ;
  questionToCreate: Question;
  disabled: string ;

  getValue(reponse: string) {
    if (reponse.length > 10 ) {
      this.alert = 'Votre question ne doit pas dépasser 10 caractères ! ';
      this.disabled = 'disabled';
    } else {
      this.alert = '';
      this.inputValue = reponse;
      this.disabled = '';
    }

  }

  getValueChoice1(choice1: string) {
    if (choice1.length > 10) {
      this.alert = 'Votre reponse ne doit pas dépasser 10 caractères ! ';
      this.disabled = 'disabled';
    } else {
      console.log(choice1);
      this.alert = '';
      this.inputValueChoice1 = choice1;
      this.disabled = '';
    }
  }

  getValueChoice2(choice2: string) {
    if (choice2.length > 10 ) {
      this.alert = 'Votre reponse ne doit pas dépasser 10 caractères ! ';
      this.disabled = 'disabled';
      console.log(choice2);
    } else {
      this.alert = '';
      this.inputValueChoice2 = choice2;
      this.disabled = '';
    }
  }

  enregistrer() {
    if (this.inputValue.substr(-1) === '?' && this.inputValue.length <= 10) {

      this.questionToCreate = new Question(this.inputValue, this.inputValueChoice1, this.inputValueChoice2);
        // ici on dit que l'ont va emettre
      this.questionCreated.emit(this.questionToCreate);

    } else if (this.inputValue === '') {
       this.finalValue === '';

    } else {
      this.alert = 'Vous n \' avez pas de ?';
    }
  }


  constructor() { }

  ngOnInit() {
  }


}
