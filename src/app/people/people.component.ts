import {Component, Input, OnInit} from '@angular/core';
import {People} from '../model/people.model';
import {StarwarsService} from '../service/starwars.service';


@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {

@Input() people: People;

  constructor( private swService: StarwarsService) {
   this.swService.getPeople().subscribe(reponse =>{
     console.log(reponse);
   });
  }

  ngOnInit() {
  }

}
