import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListQuestionComponent } from './list-question/list-question.component';
import { QuestionComponent } from './question/question.component';
import { PeopleComponent } from './people/people.component';
import { ListOfPeopleComponent } from './list-of-people/list-of-people.component';
import {Router, RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {StarwarsService} from './service/starwars.service';
import { LoaderComponent } from './loader/loader.component';
import { CreateComponent } from './create/create.component';
import {FormsModule} from '@angular/forms';

const appRoutes: Routes = [
  {path : 'question', component : ListQuestionComponent},
  {path : 'people', component : ListOfPeopleComponent},

];

@NgModule({
  declarations: [
    AppComponent,
    ListQuestionComponent,
    QuestionComponent,
    PeopleComponent,
    ListOfPeopleComponent,
    LoaderComponent,
    CreateComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes, {enableTracing: true}
    ),
    FormsModule
  ],
  // on met les services dedans
  providers: [StarwarsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
