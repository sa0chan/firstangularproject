import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ListPeople} from '../model/listPeople.model';
import {filter, map} from 'rxjs/operators';
import {People} from '../model/people.model';


@Injectable()
export class StarwarsService {
  constructor( private http: HttpClient) {
  }

// Observable
  getPeople(): Observable<People[]> {
    return this.http.get<ListPeople>('https://swapi.co/api/people').pipe(map(listPeople => listPeople.results));
  }
}



// filtrer avec un nom
/*getPeople(): Observable<People[]> {
  return this.http.get<ListPeople>('https://swapi.co/api/people').
    pipe(map(listPeople => listPeople.results.filter(list => list.name === 'Luke Skywalker'))); }
}*/
