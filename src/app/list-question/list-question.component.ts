import { Component, OnInit } from '@angular/core';
import { Question } from '../model/question';

@Component({
  selector: 'app-list-question',
  templateUrl: './list-question.component.html',
  styleUrls: ['./list-question.component.css']
})
export class ListQuestionComponent implements OnInit {
  // création d' une liste de question qu'on passera à balise question
  questions: Question[] = [];


  showQuestion(question: Question) {
    // return question !== this.questions[0] ;
    return true;
  }

  onQuestionCreated($event) {
    console.log('ceci est un event');
    console.log($event);
    this.questions.push($event);
  }

  constructor() {
  }

  ngOnInit() {
  }

}
