import { Component, OnInit } from '@angular/core';
import {People} from '../model/people.model';
import {StarwarsService} from '../service/starwars.service';
import {ListPeople} from '../model/listPeople.model';

@Component({
  selector: 'app-list-of-people',
  templateUrl: './list-of-people.component.html',
  styleUrls: ['./list-of-people.component.css']
})
export class ListOfPeopleComponent implements OnInit {

  // création d' une liste de question qu'on passera à balise question
  peoples: People[] = [];
  loader = true;



  constructor( private swService: StarwarsService) {
    // ecoute l'api
    this.swService.getPeople().subscribe(reponse => {
      reponse.forEach(people => {
        this.peoples.push(new People(people.name, people.gender));
        this.loader = false;
      });
    });

  }

  ngOnInit() {
  }

}
