import {People} from './people.model';

export class ListPeople {

  results: People[];

  constructor(listPeople) {
    this.results = listPeople;
  }

}
