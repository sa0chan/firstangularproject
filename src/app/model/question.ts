export class Question {
  enonce: string;
  reponse1: string;
  reponse2: string;


  constructor(enonce: string , reponse1: string, reponse2: string) {
    this.enonce = enonce;
    this.reponse1 = reponse1;
    this.reponse2 = reponse2 ;

  }
}
